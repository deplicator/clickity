import Phaser from 'phaser';
import './style.css';

import { Main } from './Scenes/Main';

const config = {
  type: Phaser.AUTO,
  width: 800,
  height: 600,
  scene: Main
};

const game = new Phaser.Game(config);

declare global {
  interface Window {
    game: Phaser.Game;
  }
}

window.Phaser = Phaser;
window.game = game;
