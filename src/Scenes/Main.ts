import Phaser from 'phaser';
import { Clickity } from '../Objects/Clickity';
import { IUpgradeConfig, Upgrade } from '../Objects/Upgrade';

const Clickities = [
  {
    name: 'Clickity A',
    currency: 'A',
    position: { x: 200, y: 100 },
    show: [],
    cost: []
  },
  {
    name: 'Clickity B',
    currency: 'B',
    position: { x: 500, y: 100 },
    show: [{ currency: 'A', amount: 2 }],
    cost: [{ currency: 'A', amount: 5 }]
  },
  {
    name: 'Clickity C',
    currency: 'C',
    position: { x: 350, y: 250 },
    show: [{ currency: 'B', amount: 2 }],
    cost: [
      { currency: 'A', amount: 5 },
      { currency: 'B', amount: 5 }
    ]
  }
];

const Upgrades: IUpgradeConfig[] = [
  {
    name: 'A Speed x2',
    position: { x: 150, y: 110, angle: -30 },
    show: { haveCurrency: [{ currency: 'A', amount: 3 }] },
    cost: [{ currency: 'A', amount: 5 }],
    action: {
      clickityName: 'A',
      upgradeKey: 'timeDivider',
      upgradeValue: 2
    }
  },
  {
    name: 'A Speed x4',
    position: { x: 260, y: 60, angle: 30 },
    show: {
      haveCurrency: [{ currency: 'A', amount: 10 }],
      requirement: {
        name: 'A',
        upgradeKey: 'timeDivider',
        upgradeValue: 2
      }
    },
    cost: [
      { currency: 'A', amount: 5 },
      { currency: 'B', amount: 1 }
    ],
    action: {
      clickityName: 'A',
      upgradeKey: 'timeDivider',
      upgradeValue: 4
    }
  }
];

export class Main extends Phaser.Scene {
  gameData: Phaser.Data.DataManager;
  clickities: Phaser.GameObjects.Group;
  upgrades: Phaser.GameObjects.Group;
  test: Phaser.GameObjects.Group;

  constructor() {
    super('Main');
  }

  create() {
    this.gameData = this.game.registry;

    // Create the Clickities!
    this.clickities = this.add.group({ runChildUpdate: true });
    Clickities.map((config) => this.clickities.add(new Clickity(this, config)));

    this.upgrades = this.add.group({ runChildUpdate: true });
    Upgrades.map((config, index) =>
      this.upgrades.add(new Upgrade(this, config, index))
    );
  }
}
