import { IUpgrades, noUpgrades } from './Upgrade';

export interface ICost {
  currency: string;
  amount: number;
}

export interface IClickityConfig {
  name: string;
  currency: string;
  position: { x: number; y: number };
  show: ICost[];
  cost: ICost[];
}

export type clickityState = 'available' | 'unavailable' | 'hidden';

export class Clickity extends Phaser.GameObjects.Container {
  // State
  gameState: Phaser.Data.DataManager;
  state: clickityState;
  isDisabled: boolean;
  currency: string;
  position: { x: number; y: number };
  show: ICost[];
  cost: ICost[];

  // Parts of a Clickity
  clickityButton: Phaser.GameObjects.Text;
  quantityText: Phaser.GameObjects.Text;
  progressBarOutline: Phaser.GameObjects.Rectangle;
  progressBarInside: Phaser.GameObjects.Rectangle;
  progressTimer: Phaser.Time.TimerEvent;

  // Start Defaults
  defaultState: clickityState = 'hidden';
  currentQuantity = 0;
  lifetimeQuantity = 0;
  defaultProgressTime = 1000;
  upgrades = noUpgrades;

  constructor(scene: Phaser.Scene, config: IClickityConfig) {
    super(scene);

    this.gameState = scene.game.registry;
    this.name = config.name;
    this.currency = config.currency;
    this.position = config.position;
    this.show = config.show;
    this.cost = config.cost;

    this.clickityButton = new Phaser.GameObjects.Text(
      scene,
      this.position.x,
      this.position.y,
      this.name,
      {}
    );

    this.quantityText = new Phaser.GameObjects.Text(
      scene,
      this.position.x,
      this.position.y + 20,
      '',
      {}
    );

    this.progressBarOutline = new Phaser.GameObjects.Rectangle(
      this.scene,
      this.position.x + 50,
      this.position.y + 45,
      100,
      15
    );

    this.progressBarInside = new Phaser.GameObjects.Rectangle(
      this.scene,
      this.position.x,
      this.position.y + 45,
      100,
      15
    );

    this.progressTimer = this.scene.time.delayedCall(0, undefined, [], this);

    // All all to scene
    scene.add.existing(this.clickityButton);
    scene.add.existing(this.quantityText);
    scene.add.existing(this.progressBarOutline);
    scene.add.existing(this.progressBarInside);

    // Initialize
    this.gameState.set(`${this.currency}.quantity`, 0);
    this.gameState.set(`${this.currency}.upgrades`, this.upgrades);
    this.setState(this.defaultState);
  }

  setState(state: clickityState) {
    this.state = state;
    this.isDisabled = state !== 'available';

    const buttonOpacity =
      this.state === 'hidden' ? 0 : this.state === 'unavailable' ? 0.25 : 1;

    const quantityOpacity = this.state === 'hidden' ? 0 : 1;

    this.clickityButton
      .disableInteractive()
      .off('pointerover')
      .off('pointerout')
      .off('pointerdown')
      .off('pointerup')
      .setStyle({ color: `rgb(0, 255, 0, ${buttonOpacity})` });

    this.quantityText.setStyle({
      color: `rgb(255, 255, 255, ${quantityOpacity})`
    });

    if (state !== 'hidden') {
      this.clickityButton
        .setInteractive({ useHandCursor: true })
        .on('pointerover', () =>
          this.setButtonColor(`rgb(0, 0, 255, ${buttonOpacity})`)
        )
        .on('pointerout', () =>
          this.setButtonColor(`rgb(0, 255, 0, ${buttonOpacity})`)
        )
        .on('pointerdown', () =>
          this.setButtonColor(`rgb(255, 0, 0, ${buttonOpacity})`)
        )
        .on('pointerup', () => {
          this.setButtonColor(`rgb(0, 0, 255, ${buttonOpacity})`);
          this.startClickityClick();
        })
        .setStyle({ color: `rgb(0, 255, 0, ${buttonOpacity})` });

      this.quantityText.setStyle({
        color: `rgb(255, 255, 255, ${quantityOpacity})`
      });

      this.progressBarOutline.setStrokeStyle(2, 0xff0000);

      this.progressBarInside
        .setFillStyle(0xff0000)
        .setOrigin(0, 0.5)
        .setScale(0.0, 1);
    }
  }

  startClickityClick() {
    if (!this.isDisabled && this.progressTimer.getRemaining() <= 0) {
      this.progressTimer = this.scene.time.delayedCall(
        this.defaultProgressTime /
          this.gameState.get(`${this.currency}.upgrades`).timeDivider,
        () => {
          this.cost.forEach((c) =>
            this.gameState.inc(`${c.currency}.quantity`, -c.amount)
          );
          this.gameState.inc(`${this.currency}.quantity`, this.upgrades.power);
        },
        [],
        this
      );
    }
  }

  update() {
    this.updateClickCountText();

    // Move progress bar by setting inside rectangle scale
    this.progressBarInside.setScale(this.progressTimer.getProgress(), 1);
    if (this.progressTimer.getProgress() === 1) {
      this.progressBarInside.setScale(0, 1);
    }

    // Check currency quantities to show Clickity
    if (
      this.state === 'hidden' &&
      this.show.every(
        (s) => this.gameState.get(`${s.currency}.quantity`) >= s.amount
      )
    ) {
      this.setState('unavailable');
    }

    // Check currency quantities to enable Clickity
    if (
      this.state === 'unavailable' &&
      this.cost.every(
        (s) => this.gameState.get(`${s.currency}.quantity`) >= s.amount
      )
    ) {
      this.setState('available');
    }

    // Disable if cannot buy
    if (
      this.state === 'available' &&
      this.cost.length !== 0 &&
      this.cost.every(
        (s) => this.gameState.get(`${s.currency}.quantity`) < s.amount
      )
    ) {
      this.setState('unavailable');
    }
  }

  updateClickCountText() {
    this.quantityText.setText(
      `Quantity: ${this.gameState.get(`${this.currency}.quantity`)}`
    );
  }

  setButtonColor(color: string) {
    if (!this.isDisabled) {
      this.clickityButton.setStyle({ color });
    }
  }

  getCount() {
    return this.currentQuantity;
  }

  useClickity(quantity: number) {
    if (this.currentQuantity >= quantity) {
      this.currentQuantity = this.currentQuantity - quantity;
      this.updateClickCountText();
      return true;
    }
    return false;
  }

  toggleDisabled() {
    console.log('toggle');
  }

  getState() {
    return this.state;
  }

  setUpgrade(upgradeKey: keyof IUpgrades, value: unknown) {
    this.upgrades[upgradeKey] = value;
    this.setState(this.getState());
  }
}
