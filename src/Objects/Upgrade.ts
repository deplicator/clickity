import { Main } from '../Scenes/Main';
import { ICost } from './Clickity';

export interface IUpgrades {
  automated: boolean;
  power: number;
  timeDivider: number;
  [key: string]: unknown;
}

export interface IShow {
  haveCurrency?: ICost[];
  requirement?: {
    name: string;
    upgradeKey: keyof IUpgrades;
    upgradeValue: unknown;
  };
}

export const noUpgrades: IUpgrades = {
  automated: false,
  power: 1,
  timeDivider: 1
};

export interface IUpgradeConfig {
  name: string;
  position: { x: number; y: number; angle?: number };
  show: IShow;
  cost: ICost[];
  action: {
    clickityName: string;
    upgradeKey: keyof IUpgrades;
    upgradeValue: unknown;
  };
}

export class Upgrade extends Phaser.GameObjects.Container {
  callback: CallableFunction;
  text: Phaser.GameObjects.Text;

  defaultState = 'hidden';
  isDisabled: boolean;
  show: IShow;
  cost: ICost[];
  gameState: Phaser.Data.DataManager;
  action: {
    clickityName: string;
    upgradeKey: keyof IUpgrades;
    upgradeValue: unknown;
  };

  index: number;

  constructor(scene: Main, config: IUpgradeConfig, index: number) {
    super(scene);

    this.gameState = scene.game.registry;
    this.show = config.show;
    this.cost = config.cost;
    this.action = config.action;
    this.name = config.name;
    this.index = index;

    this.text = new Phaser.GameObjects.Text(
      scene,
      config.position.x,
      config.position.y,
      config.name,
      {}
    );

    this.text.setAngle(config.position.angle ? config.position.angle : 0);

    scene.add.existing(this.text);

    this.setState(this.defaultState);
  }

  setState(state: string) {
    this.state = state;
    this.isDisabled = state !== 'available';

    const opacity =
      this.state === 'hidden' ? 0 : this.state === 'unavailable' ? 0.25 : 1;

    this.text
      .disableInteractive()
      .off('pointerover')
      .off('pointerout')
      .off('pointerdown')
      .off('pointerup')
      .setStyle({ color: `rgb(0, 255, 0, ${opacity})` });

    if (state !== 'hidden') {
      this.text
        .disableInteractive()
        .setInteractive({ useHandCursor: true })
        .on('pointerover', () =>
          this.setButtonColor(`rgb(0, 0, 255, ${opacity})`)
        )
        .on('pointerout', () =>
          this.setButtonColor(`rgb(0, 255, 0, ${opacity})`)
        )
        .on('pointerdown', () =>
          this.setButtonColor(`rgb(255, 0, 0, ${opacity})`)
        )
        .on('pointerup', () => {
          this.setButtonColor(`rgb(0, 0, 255, ${opacity})`);
          this.startClickityClick();
        })
        .setStyle({ color: `rgb(0, 255, 0, ${opacity})` });
    }
  }

  startClickityClick() {
    if (!this.isDisabled) {
      this.cost.forEach((c) =>
        this.gameState.inc(`${c.currency}.quantity`, -c.amount)
      );
      const currentUpgrades = this.gameState.get(
        `${this.action.clickityName}.upgrades`
      );
      this.gameState.set(`${this.action.clickityName}.upgrades`, {
        ...currentUpgrades,
        [this.action.upgradeKey]: this.action.upgradeValue
      });

      // I don't get why this.destroy() doesn't do anything
      this.text
        .disableInteractive()
        .off('pointerover')
        .off('pointerout')
        .off('pointerdown')
        .off('pointerup')
        .setVisible(false);
    }
  }

  checkShowRequirements() {
    let rtnValue = false;
    if (this.show.haveCurrency) {
      rtnValue = this.show.haveCurrency.every(
        (s) => this.gameState.get(`${s.currency}.quantity`) >= s.amount
      );
    }
    if (this.show.requirement) {
      rtnValue =
        rtnValue &&
        this.gameState.get(`${this.show.requirement.name}.upgrades`)[
          this.show.requirement.upgradeKey
        ] === this.show.requirement.upgradeValue;
    }
    return rtnValue;
  }

  update() {
    // Show when show cost met
    if (this.state === 'hidden' && this.checkShowRequirements()) {
      this.setState('unavailable');
    }

    // Check currency quantities to enable Upgrade
    if (
      this.state === 'unavailable' &&
      this.cost.every(
        (s) => this.gameState.get(`${s.currency}.quantity`) >= s.amount
      )
    ) {
      this.setState('available');
    }

    // Disable if cannot buy
    if (
      this.state === 'available' &&
      this.cost.length !== 0 &&
      this.cost.every(
        (s) => this.gameState.get(`${s.currency}.quantity`) < s.amount
      )
    ) {
      this.setState('unavailable');
    }
  }

  setButtonColor(color: string) {
    if (!this.isDisabled) {
      this.text.setStyle({ color });
    }
  }
}
